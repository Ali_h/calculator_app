package com.example.ali.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button0, button1, button2, button3, button4, button5, button6, button7, button8,
            button9, buttonDot, buttonEquals, buttonDivide, buttonMultiple, buttonPlus,
            buttonMinus, buttonClr, buttonNeg;
    TextView operation;
    EditText resualt, input;
    ImageButton clearResualt , clearInput;

    private Double holderOperand = null;
    private String holderOperation = "=";

    private static final String SHOW_OPERATION_STATE = "showOperationState";
    private static final String STATE_OPERAND = "stateOperand";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        operation = (TextView) findViewById(R.id.operation);
        resualt = (EditText) findViewById(R.id.resualt);
        input = (EditText) findViewById(R.id.input);
        clearInput = (ImageButton) findViewById(R.id.imageButtonClrInput);
        clearResualt = (ImageButton) findViewById(R.id.imageButtonClrResualt);

        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);

        buttonDot = (Button) findViewById(R.id.buttonDot);
        buttonDivide = (Button) findViewById(R.id.buttonDivide);
        buttonMultiple = (Button) findViewById(R.id.buttonMultiple);
        buttonMinus = (Button) findViewById(R.id.buttonMinus);
        buttonEquals = (Button) findViewById(R.id.buttonEquals);
        buttonClr = (Button) findViewById(R.id.buttonClr);
        buttonNeg = (Button) findViewById(R.id.buttonNegative);
        buttonPlus = (Button) findViewById(R.id.buttonPlus);

        button0.setOnClickListener(operand);
        button0.setOnLongClickListener(operandLongClick);
        button1.setOnClickListener(operand);
        button1.setOnLongClickListener(operandLongClick);
        button2.setOnClickListener(operand);
        button2.setOnLongClickListener(operandLongClick);
        button3.setOnClickListener(operand);
        button3.setOnLongClickListener(operandLongClick);
        button4.setOnClickListener(operand);
        button4.setOnLongClickListener(operandLongClick);
        button5.setOnClickListener(operand);
        button5.setOnLongClickListener(operandLongClick);
        button6.setOnClickListener(operand);
        button6.setOnLongClickListener(operandLongClick);
        button7.setOnClickListener(operand);
        button7.setOnLongClickListener(operandLongClick);
        button8.setOnClickListener(operand);
        button8.setOnLongClickListener(operandLongClick);
        button9.setOnClickListener(operand);
        button9.setOnLongClickListener(operandLongClick);
        buttonDot.setOnClickListener(operand);
        buttonDot.setOnLongClickListener(operandLongClick);

        //BUTTON NEGATIVE ---------------------------------------------------------------------
        buttonNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = input.getText().toString();
                if (value.length() == 0) {
                    input.setText("-");
                } else {
                    try {
                        Double doubleValue = Double.valueOf(value);
                        doubleValue *= -1;
                        input.setText(doubleValue.toString());
                    } catch (NumberFormatException error) {
                        input.setText("");
                    }
                }
            }
        });
        buttonNeg.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return false;
            }
        });
        //-------------------------------------------------------------------------------------

        //BUTTON CLEAR ------------------------------------------------------------------------
        buttonClr.setOnClickListener(clearListener);
        buttonClr.setOnLongClickListener(clearListenerLogListener);
        clearInput.setOnClickListener(clearListener);
        clearInput.setOnLongClickListener(clearListenerLogListener);
        clearResualt.setOnClickListener(clearListener);
        clearResualt.setOnLongClickListener(clearListenerLogListener);
        //-------------------------------------------------------------------------------------
        
        //BUTTON OPERATION
        buttonPlus.setOnClickListener(operationListener);
        buttonMinus.setOnClickListener(operationListener);
        buttonMultiple.setOnClickListener(operationListener);
        buttonDivide.setOnClickListener(operationListener);
        buttonEquals.setOnClickListener(operationListener);
        buttonPlus.setOnLongClickListener(operationListenerLongClick);
        buttonMinus.setOnLongClickListener(operationListenerLongClick);
        buttonMultiple.setOnLongClickListener(operationListenerLongClick);
        buttonDivide.setOnLongClickListener(operationListenerLongClick);
        buttonEquals.setOnLongClickListener(operationListenerLongClick);
        //------------------------------------------------------------------------------------

    }

    //CLEAR --------------------------------------------------------------------------
    View.OnClickListener clearListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.imageButtonClrInput:
                    input.setText("");
                    break;
                case R.id.imageButtonClrResualt:
                    resualt.setText("0.0");
                    holderOperand = null;
                    break;
            }
        }
    };
    View.OnLongClickListener clearListenerLogListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            return false;
        }
    };
    //---------------------------------------------------------------------------------

    //OPERAND ------------------------------------------------------------------------
    View.OnClickListener operand = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Button castButton = (Button) view;
            input.append(castButton.getText().toString());
        }
    };
    View.OnLongClickListener operandLongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            return false;
        }
    };
    //---------------------------------------------------------------------------------
    
    //OPERATION ----------------------------------------------------------------------
    View.OnClickListener operationListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Button operationButton = (Button) view;
            String operationValue = operationButton.getText().toString();
            String inputValue = input.getText().toString();
            try{
                Double inputDoubleValue = Double.valueOf(inputValue);
                handlerOperation(inputDoubleValue , operationValue);
            } catch (NumberFormatException error){
                input.setText("");
            }
            holderOperation = operationValue;
            operation.setText(holderOperation);
        }
    };
    View.OnLongClickListener operationListenerLongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            return false;
        }
    };
    //---------------------------------------------------------------------------------

    private void handlerOperation(Double value, String operationValue) {
        if (holderOperand == null){
            holderOperand = value;
        }else{
            if (holderOperation.equals("=")){
                holderOperation = operationValue;
            }
            switch (holderOperation){
                case "=":
                    holderOperand = value;
                    break;
                case "+":
                    holderOperand += value;
                    break;
                case "-":
                    holderOperand -= value;
                    break;
                case "*":
                    holderOperand *= value;
                    break;
                case "/":
                    if (value == 0){
                        input.setText(R.string.divideZero);
                    }
                    holderOperand /= value;
                    break;
            }
        }
        String resualtShow = holderOperand.toString();
        resualt.setText(resualtShow);
        input.setText("");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(SHOW_OPERATION_STATE , holderOperation);
        if (holderOperand != null){
            outState.putDouble(STATE_OPERAND , holderOperand);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        holderOperation = savedInstanceState.getString(SHOW_OPERATION_STATE);
        holderOperand = savedInstanceState.getDouble(STATE_OPERAND);
        operation.setText(holderOperation);
    }
}
